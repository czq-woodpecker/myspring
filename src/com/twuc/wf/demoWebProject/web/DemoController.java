package com.twuc.wf.demoWebProject.web;

import com.twuc.wf.twspring.annotations.RequestMapping;
import com.twuc.wf.twspring.annotations.RequestParam;
import com.twuc.wf.twspring.annotations.ResponseStatus;
import com.twuc.wf.twspring.annotations.RestController;
import com.twuc.wf.twspring.simplehttpserver.contract.HttpStatus;
import com.twuc.wf.twspring.simplehttpserver.contract.RequestMethod;

@RestController("/api")
public class DemoController {

    @RequestMapping(value = "/demo", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public String demoAction(@RequestParam("aaa") String name, @RequestParam("bbb") String age){
        throw new RuntimeException();
//        return String.format("hello %s, seems you are %s years old.", name,age);
    }
}
