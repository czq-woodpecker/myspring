package com.twuc.wf.twspring.simplehttpserver;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.twuc.wf.twspring.constant.CONSTANT.HTTP_SERVER_LISTEN_ON_PORT;
import static com.twuc.wf.twspring.constant.CONSTANT.pInfo;

public class SimpleHttpServer implements IServer {
    private int port;

    ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    private final ServerSocket serverSocket;

    public SimpleHttpServer(int http_listen_port) throws IOException {
        this.port = http_listen_port;
        serverSocket = new ServerSocket(this.port);
    }


    @Override
    public void start() throws IOException {
        pInfo("[ "+Thread.currentThread().getName()+" ] " + String.format(HTTP_SERVER_LISTEN_ON_PORT,8080));
        while(!Thread.interrupted()){
            Socket socket = this.serverSocket.accept();
            executorService.execute(new SimpleHttpHandler(socket));
        }
    }

    @Override
    public void stop() {

    }

    @Override
    public void restart() {

    }
}
