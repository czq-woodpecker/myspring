package com.twuc.wf.twspring.core;

import com.twuc.wf.twspring.simplehttpserver.IServer;
import com.twuc.wf.twspring.simplehttpserver.SimpleHttpServer;
import com.twuc.wf.twspring.simplehttpserver.contract.RequestMethod;
import com.twuc.wf.twspring.annotations.ControllerAdvice;
import com.twuc.wf.twspring.annotations.RequestMapping;
import com.twuc.wf.twspring.annotations.RestController;
import com.twuc.wf.twspring.entity.RouteEntity;
import com.twuc.wf.twspring.utils.ClassUtil;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.twuc.wf.twspring.constant.CONSTANT.VERSION;
import static com.twuc.wf.twspring.constant.CONSTANT.pInfo;


public class SpringApplication {

    private int HTTP_LISTEN_PORT = 8080;


    public static Map<String, RouteEntity>  getMappingMap;
    public static Map<String, RouteEntity>  postMappingMap;
    public static Map<Class<?>, Class<?>>  exceprionHandlerMap;


    public void run(Class<?> contextClz){
        printCoolStartArt();

        // 1. 服务扫描 & 服务注册
        initServices(contextClz);

        // 3. http server 启动
        try {
            startHttpListener();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void printCoolStartArt() {
        System.out.println(" _____               _____               _               \n" +
                        "|_   _|             /  ___|             (_)              \n" +
                        "  | |  __      __   \\ `--.  _ __   _ __  _  _ __    __ _ \n" +
                        "  | |  \\ \\ /\\ / /    `--. \\| '_ \\ | '__|| || '_ \\  / _` |\n" +
                        "  | |   \\ V  V /  _ /\\__/ /| |_) || |   | || | | || (_| |\n" +
                        "  \\_/    \\_/\\_/  (_)\\____/ | .__/ |_|   |_||_| |_| \\__, |\n" +
                        "                           | |                      __/ |\n" +
                        "                           |_|                     |___/ \n"+
                VERSION+"\n");
    }

    private void startHttpListener() throws IOException {
        IServer server = new SimpleHttpServer(HTTP_LISTEN_PORT);
        server.start();
    }

    private void initServices(Class<?> contextClz) {
        getMappingMap = new HashMap<>();
        postMappingMap = new HashMap<>();
        exceprionHandlerMap = new HashMap<>();

        List<Class<?>> classes = ClassUtil.getAllClassByPackageName(contextClz.getPackage());

        for(Class<?> c:classes){

            // scan @RestController
            RestController restController = c.getDeclaredAnnotation(RestController.class);
            if(null != restController){
                Method[] methods = c.getMethods();
                for(Method m:methods){
                    RequestMapping requestMapping = m.getDeclaredAnnotation(RequestMapping.class);
                    if(null != requestMapping){
                        if (requestMapping.method() == RequestMethod.POST) {
                            postMappingMap.put(restController.value() + requestMapping.value(), new RouteEntity(c, m));
                        } else {
                            getMappingMap.put((restController.value() + requestMapping.value()).split("\\?")[0], new RouteEntity(c, m));
                        }
                    }
                }
            }

            // scan @ControllerAdvice
            ControllerAdvice controllerAdvice = c.getDeclaredAnnotation(ControllerAdvice.class);
            if(null != controllerAdvice){
                for(Class<?> packageClz:controllerAdvice.basePackagesClasses()) {
                    exceprionHandlerMap.put(packageClz, c);
                }
            }
        }

        pInfo("[ "+Thread.currentThread().getName()+" ] " + getMappingMap.toString());
        pInfo("[ "+Thread.currentThread().getName()+" ] " + postMappingMap.toString());
        pInfo("[ "+Thread.currentThread().getName()+" ] " + exceprionHandlerMap.toString());
    }
}
