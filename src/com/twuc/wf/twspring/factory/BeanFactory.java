package com.twuc.wf.twspring.factory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BeanFactory {


    static Map<Class<?>, Object> beans = new ConcurrentHashMap<>();

    public static Object getBean(Class<?> clz) throws IllegalAccessException, InstantiationException {
        Object object = beans.get(clz);
        if(null==object){
            object = clz.newInstance();
            beans.put(clz,object);
            return object;
        }
        return beans.get(clz);
    }

}
